public class Point {
    public double x;
    public double y;

    public Point(double x, double y){
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object other){
        if (other instanceof Point){
            Point oth = (Point) other;
            if(this.x == oth.x && this.y == oth.y){
                return true;
            }
            else {
                return false;
            }
        }
        else{
            return false;
        }
    }
}
