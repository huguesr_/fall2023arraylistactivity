import java.util.*;

public class ListPractice{

    public static void main(String[] args){
        // String[] strings = {"APPLE", "hello", "WHERE", "opp", "pear"};

        // List<String> words = new ArrayList<String>();
        // System.out.println(words.size());

        // for (String s : strings){
        //     words.add(s);
        // }
        // System.out.println(words.size());

        // System.out.println(words.contains("opp"));
        // System.out.println(words.contains("hahahaha"));

        // List<String> upperWords = getUpperCase(words);
        // for (int i =0; i<upperWords.size() ; i++){
        //      System.out.println(upperWords.get(i));
        // }

        ArrayList<Point> points = new ArrayList<Point>();
        Point n1 = new Point(2.3, 3.4);
        Point n2 = new Point(5.7, 1);
        Point n3 = new Point(9.2, 5.6);
        Point target = new Point(9.2, 5.6);
        points.add(n1);
        points.add(n2);
        points.add(n3);

        System.out.println(points.contains(target));
    }

    public static int countUpperCase(String[] strings){
        int upperCount =0;
        for (int i =0; i<strings.length ; i++){
            if (strings[i].matches("[A-Z]*")){
                upperCount++;
            }
        }
        return upperCount;
    }

    public static String[] getUpperCase(String[] strings){
        int upperCount =0;
        for (int i =0; i<strings.length ; i++){
            if (strings[i].matches("[A-Z]*")){
                upperCount++;
            }
        }
        String[] newS = new String[upperCount];
        int newInd = 0;
        for (int j =0; j<strings.length ; j++){
            if (strings[j].matches("[A-Z]*")){
                newS[newInd] = strings[j];
                newInd++;
            }
        }
        return newS;
    }

//-------------------------------overloaded--------------------------------//
    public static List<String> getUpperCase(List<String> words){
        List<String> newW = new ArrayList<String>();
        for (int i =0; i<words.size() ; i++){
            if(words.get(i).matches("[A-Z]*")){
                newW.add(words.get(i));
            }
        }
        return newW;
    }
}